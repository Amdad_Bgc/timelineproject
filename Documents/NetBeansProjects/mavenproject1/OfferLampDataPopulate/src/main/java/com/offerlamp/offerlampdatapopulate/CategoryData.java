/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.offerlamp.offerlampdatapopulate;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import offerlamp.entity.Category;
import offerlamp.entity.SubCategory;
import offerlamp.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Amdad Hossain
 */
public class CategoryData {
     Long categoryId;
     Category categoryData;
     
     
public static void main(String[] args) {
       
        new CategoryData().insertCategorySubCategory();
    }

private void insertCategorySubCategory()
{
    //open session
    Session session=HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();
    
    //csv file path
     String csvFile = "F:\\java/csv/category_subcategory.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
     try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] category = line.split(cvsSplitBy);
             // System.out.println("Data [category= " + category[0] + " , status=" + category[1] + ", subcategory=" + category[2] + ", status=" + category[3] + "]");

                Date date=new Date();
                //categorydata from file
                String categoryName=category[0];
                String status=category[1];
    
                //subcategory data from file
                String subCategoryName=category[2];
                String subCategoryStatus=category[3];
               
                
                
                 //check same categoryname in category table
                 Criteria criteria=session.createCriteria(Category.class);
                 criteria.add(Restrictions.eq("name", categoryName));
                 Category categoryNameCheck=(Category)criteria.uniqueResult();
                
                //if it added new category
                if(categoryNameCheck==null)
                 {
                      categoryData = new Category(categoryName,date,status);
                     session.save(categoryData);
                     //criteria.setProjection(Projections.rowCount());
                     //long categoryId=(Long)criteria.uniqueResult();
                     long categoryId=categoryData.getId();
                     SubCategory subcategoryData = new SubCategory(categoryId,subCategoryName,date,subCategoryStatus);
                     session.save(subcategoryData);

                 }
                //if it has the same category only add subcategory based on this category
                else
                {
                    long categoryId=categoryData.getId();
                    SubCategory subcategoryData = new SubCategory(categoryId,subCategoryName,date,subCategoryStatus);
                    session.save(subcategoryData);           
                }
            }

            session.getTransaction().commit();
            session.close();    
    
            System.out.print("Insert Successfull");
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        
   
}
}
