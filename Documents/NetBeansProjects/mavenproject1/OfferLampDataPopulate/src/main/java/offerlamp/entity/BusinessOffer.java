package offerlamp.entity;
// Generated Nov 26, 2016 10:00:12 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * BusinessOffer generated by hbm2java
 */
public class BusinessOffer  implements java.io.Serializable {


     private Long id;
     private long business;
     private String title;
     private String details;
     private String image;
     private Date startTime;
     private Date endTime;
     private Date createdAt;
     private boolean canUpdate;

    public BusinessOffer() {
    }

    public BusinessOffer(long business, String title, String details, String image, Date startTime, Date endTime, Date createdAt, boolean canUpdate) {
       this.business = business;
       this.title = title;
       this.details = details;
       this.image = image;
       this.startTime = startTime;
       this.endTime = endTime;
       this.createdAt = createdAt;
       this.canUpdate = canUpdate;
    }
   
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public long getBusiness() {
        return this.business;
    }
    
    public void setBusiness(long business) {
        this.business = business;
    }
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDetails() {
        return this.details;
    }
    
    public void setDetails(String details) {
        this.details = details;
    }
    public String getImage() {
        return this.image;
    }
    
    public void setImage(String image) {
        this.image = image;
    }
    public Date getStartTime() {
        return this.startTime;
    }
    
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    public Date getEndTime() {
        return this.endTime;
    }
    
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    public Date getCreatedAt() {
        return this.createdAt;
    }
    
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    public boolean isCanUpdate() {
        return this.canUpdate;
    }
    
    public void setCanUpdate(boolean canUpdate) {
        this.canUpdate = canUpdate;
    }




}


