package db_javatest.entity;
// Generated Nov 26, 2016 10:00:12 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Category generated by hbm2java
 */
public class Category  implements java.io.Serializable {


     private Long id;
     private String name;
     private Date createdAt;
     private String status;

    public Category() {
    }

    public Category(String name, Date createdAt, String status) {
       this.name = name;
       this.createdAt = createdAt;
       this.status = status;
    }
   
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public Date getCreatedAt() {
        return this.createdAt;
    }
    
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }




}


